package com.elementary.backend;

public class ResultMessage {

    private String data;

    public ResultMessage(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}